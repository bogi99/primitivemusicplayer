<?php
    $files = glob("*.{mp3,ogg}",GLOB_BRACE);
    // print_r($files);
?>
<!DOCTYPE html>
<html>
<head>
    <title>primitive music player</title>
    <meta charset="UTF-8" >
	<style media="screen" type="text/css">
		.fonta 
		{
			font-family: sans-serif;
			font-size: 12px;
		}
	
		.serif     { font-family: Times, Times New Roman, Georgia, serif; }
		.sansserif { font-family: Verdana, Arial, Helvetica, sans-serif; }
		.monospace { font-family: Lucida Console, Courier, monospace; }
		.cursive   { font-family: cursive; }
		.fantasy   { font-family: fantasy; }
		
		.fs08 { font-size: 8px ; }
		.fs12 { font-size: 12px; }
		.fs16 { font-size: 16px; }
		.fs24 { font-size: 24px; }
		
	</style>
</head>
<body>
    <div style="width: 100% ; height: 25px; " class=" serif fs24 " >A simplistic audio player using php and html5 </div>
<?php
    foreach ($files as $f)
    {
	echo "
	<div style=\"text-align: left; float: left; \" class=\" monospace fs16 \">\n
	<audio controls=\"controls\" style=\"width: 1000px; background-color: gray; border: 1px; border-radius: 4px; \"  >\n
	<source src=\"$f\" type=\"audio/mpeg\">\n
	</audio>\n
	</div>\n
	<div style=\"float: left;\" class=\" serif fs16 \" >&nbsp $f</div>\n
	\n";
	echo "<div style=\"height: 10px ; width: 100% ; background-color: white; float: left; \">&nbsp</div>\n\n";
	
    }
?>

</body>
</html>
