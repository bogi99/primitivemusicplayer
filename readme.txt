Simplistic audio player
-----------------------

This is a very simplistic audio player that runs on a local web server, any kind really, 
you simply drop index.php in the folder alongside a bunch of mp3 files and you get a list 
of audio files to play through your browser. 
Great for listening to your podcast dump or other un-categorized.

Just drop your mp3 or ogg files files in the same folder, and refresh your browser.

Notes: web server

short of having your installation of Apache or Nginx you may 
just run the php built in webserver, your minimum requirement is PHP 5.4.0.

cd to_the_folder_that_contains_the_player_and_your_audio_files

then start the local server:
php -S localhost:8000

** port 8000 is generally a good pick, it is non-privileged, 
so any user can use it, it is also likely to be free, 
not used by any other service. But do check your system.

after the successful start, 
you can point your browser to http://localhost:8000 and enjoy your audio files
